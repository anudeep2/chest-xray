# IMPORT KERAS LIBRARY
from keras.applications.inception_v3 import InceptionV3
from keras.applications.densenet import DenseNet121
from keras.preprocessing import image
from keras.models import Model
from keras.layers import Dense, Input, GlobalAveragePooling2D, MaxPooling2D, Flatten, BatchNormalization, Dropout
from keras.layers import GlobalMaxPooling2D, Convolution2D, AveragePooling2D, Concatenate, Activation
from keras.utils.data_utils import get_file
from keras.callbacks import ModelCheckpoint
from keras import regularizers
from keras import backend as K
from sklearn.preprocessing import MultiLabelBinarizer
import tensorflow as tf
import pandas as pd
import os
import datetime

import numpy as np
import cv2



"""
    Preprocess image
"""

def preprocess_img(image):
    image = image[:,:,0].astype(np.uint8)
    image = cv2.equalizeHist(image)
    image = cv2.GaussianBlur(image,(3, 3), 0)
    image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)


    # normalization
    image = image.astype(np.float32) - 128.
    image /= 64.
    #print(np.unique(image))
    return image

def preprocess_img_valid(image):
    image = image[:,:,0].astype(np.uint8)
    image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)


    # normalization
    image = image.astype(np.float32) - 128.
    image /= 64.
    #print(np.unique(image))
    return image
    

"""
    Custom loss function
"""
_epsilon = 1e-7

def calculating_class_weights(y_true):
    from sklearn.utils.class_weight import compute_class_weight
    number_dim = np.shape(y_true)[1]
    weights = np.empty([number_dim, 2])
    for i in range(number_dim):
        weights[i] = compute_class_weight('balanced', [0.,1.], y_true[:, i])
    return weights

weights = None

def weighted_loss(y_true, y_pred):
    y_pred = K.clip(y_pred, K.epsilon(), 1.0-K.epsilon())
    # cross-entropy loss with weighting
    out = -(y_true * K.log(y_pred)*(weights[:,1]) + (1.0 - y_true) * K.log(1.0 - y_pred)*(weights[:,0]))
    return K.mean(out, axis=-1)
    
    #print(weights)
    #return K.mean((weights[:,0]**(1-y_true))*(weights[:,1]**(y_true))*K.binary_crossentropy(y_true, y_pred), axis=-1)



def weighted_bce_loss(y_true,y_pred):
    loss = -(pos_weights*(y_true*K.log(y_pred + _epsilon)) + ((1-y_true)*K.log(1-y_pred + _epsilon)))
    return loss


def lr_scheduler(epoch, lr):
    decay_rate = 0.96
    decay_step = 1
    if epoch % decay_step == 0 and epoch:
        return lr * decay_rate
    return lr




"""
    DenseNet121 based classifier
"""

    
class Classifier:
    def __init__(self, image_size, num_gpus, num_classes, batch_size, experiment_group="tryouts", experiment_title="None"):
        self.img_size = image_size
        self.num_gpus = num_gpus
        self.num_classes = num_classes
        self.batch_size = batch_size
        self.experiment_group = experiment_group
        self.experiment_title = experiment_title

    def build_model(self, load_previous_weights = False, freeze_cnn = False, if_grad = False):
        image_size = self.img_size
        logdir = "../logs/" + self.experiment_group + '/' + self.experiment_title

        #strategy = tf.distribute.MirroredStrategy()
        #print('Number of devices: {}'.format(strategy.num_replicas_in_sync))
        #with strategy.scope():
        base_model = DenseNet121(include_top= False, input_shape=(image_size,image_size,3), weights='imagenet')
        cam_out = base_model.layers[-1].output

        """ 
            Load pretrained weights
        """
        # add a global spatial average pooling layer
        x = base_model.output
        x = GlobalAveragePooling2D(input_shape=(1024,1,1))(x)
        # Add a flattern layer 
        x = Dense(2048, activation='relu')(x)
        x = BatchNormalization()(x)
        x = Dropout(0.2)(x)
        # Add a fully-connected layer
        x = Dense(512, activation='relu')(x)
        x = BatchNormalization()(x)
        x = Dropout(0.2)(x)


        # and a logistic layer --  we have 5 classes
        temp_predictions = Dense(6, activation='sigmoid')(x)
        # this is the model we will train
        base_model2 = Model(inputs=base_model.input, outputs=temp_predictions)
        del base_model  
        #base_model2.load_weights("../weights.hdf5")#model_weights/Chex+Cov/9cls_BCE_sigmoid_32.hdf5")
        print("\n\n Pretrained weights Loaded\n\n")

        output = Dense(self.num_classes, activation='sigmoid')(base_model2.layers[-2].output)

        if if_grad:
            model =  Model(inputs=base_model2.input, outputs=[output, cam_out])

        else:
            model = Model(inputs=base_model2.input, outputs=output)

        if freeze_cnn:
            for layer in model.layers[:-2]:
                layer.trainable = False

        del base_model2


#             # fc_1x1_conv

#             fc_convs = []
#             outs = []

#             for k in range(self.num_classes):
#                 fc_convs.append(tf.keras.layers.Conv2D(1, 1, activation='sigmoid'))  

#             # PCAM pooling

#             for k in range(self.num_classes):
#                 # Results in a 7x7 CAM
#                 CAM = fc_convs[k](cam_out)
#                 CAM = tf.math.divide(CAM, tf.reduce_sum(CAM))

#                 # pooling:
#                 print(cam_out, CAM, fc_convs[0])
#                 reshaped_cam_out = tf.reshape(cam_out, [tf.shape(cam_out)[0], cam_out.get_shape()[1]*cam_out.get_shape()[2]
#                                                         , cam_out.get_shape()[-1]])
#                 reshaped_CAM = tf.reshape(CAM, [tf.shape(CAM)[0], CAM.get_shape()[1]*CAM.get_shape()[2]
#                                                         , CAM.get_shape()[-1]])
#                 print(reshaped_cam_out, reshaped_CAM)
#                 pool = tf.keras.layers.Dot(axes=(1))([reshaped_cam_out, reshaped_CAM])
#                 pool = tf.reshape(pool, [tf.shape(pool)[0], 1, 1, pool.get_shape()[1]])
#                 final_out = tf.keras.layers.Flatten()(fc_convs[k](pool))
#                 outs.append(final_out)

#             output = tf.concat(outs, axis=-1)

#             model = Model(inputs=base_model2.input, outputs=output)
            
        print(model.input, model.output)
            
        #del base_model2

        #model.summary()


        # compile the model (should be done *after* setting layers to non-trainable)
        #lr = e-2 when conv layers are frozen, e-4 when training in encoder is allowed
#         optimizer = tf.keras.optimizers.Adam(learning_rate=1e-4)
#         model.compile(optimizer=optimizer, loss='binary_crossentropy', 
#                       metrics=['accuracy', tf.keras.metrics.AUC(name="ROC", multi_label=True),
#                                tf.keras.metrics.AUC(name="PR", curve='PR', multi_label=True)])

        self.tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=logdir, write_graph=False, histogram_freq=0,
                                                                update_freq='epoch')
        self.model = model

        

    def train_model(self, epochs=1):
        self.lr_callback = tf.keras.callbacks.LearningRateScheduler(lr_scheduler)
        self.reduce_lr = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.2,
                            patience=3, min_delta=0.0001, verbose=1, cooldown=0, min_lr=0)

        if not os.path.exists('../model_weights/' + self.experiment_group):
            os.mkdir('../model_weights/' + self.experiment_group)
        
        self.checkpointer = ModelCheckpoint(filepath='../model_weights/' + self.experiment_group + '/' +
                                            self.experiment_title + '{epoch:02d}.hdf5', 
                                            verbose=1, save_best_only=True)
        
        
        STEP_SIZE_TRAIN=self.train_generator.n//self.train_generator.batch_size
        STEP_SIZE_VALID=self.validation_generator.n//self.validation_generator.batch_size

        #print(self.train_generator.class_indices)

        history = self.model.fit_generator(generator=self.train_generator,
                                           steps_per_epoch=int(STEP_SIZE_TRAIN),
                                           validation_data=self.validation_generator,
                                           validation_steps=STEP_SIZE_VALID,
                                           max_queue_size=300,
                                           workers=20,
                                           validation_freq = 1,
                                           verbose=1,
                                           epochs=epochs,
                                           #initial_epoch=,
                                           callbacks = [self.checkpointer, self.tensorboard_callback, self.lr_callback, self.reduce_lr])
        return history
    
    
    def datagen_init_train_old(self, df, data_path):                                                                                                                             
        datagen = image.ImageDataGenerator(
                                 rescale=1./255,
                                 featurewise_center=True,
                                 featurewise_std_normalization=True,
                                 rotation_range=15,
                                 zoom_range=0.05,
                                 width_shift_range=0.2,
                                 height_shift_range=0.2,
                                 horizontal_flip=True,
                                 zca_whitening=True)
                                 #preprocessing_function=preprocess_img)
                                        
                                 #zca_whitening=True)

        self.train_generator = datagen.flow_from_dataframe(dataframe=df, directory=data_path, 
                                                x_col="Path", y_col="feature_string", seed = 42, shuffle=True,
                                                class_mode="categorical", target_size=(self.img_size,self.img_size),
                                                batch_size=self.batch_size, subset = "training") 
        
        
        test = pd.Series(self.train_generator.labels)
        mlb = MultiLabelBinarizer()
        y_labels = mlb.fit_transform(test)
        
        global weights
        weights = calculating_class_weights(y_labels)
        print("\n\n", weights, "\n\n")
        
    
    def datagen_init_valid_old(self, df, data_path):

        datagen_test=image.ImageDataGenerator(rescale=1./255)#preprocessing_function=preprocess_img_valid)#rescale=1./255

        self.validation_generator=datagen_test.flow_from_dataframe(dataframe=df, directory=data_path, 
                                                x_col="Path", y_col="feature_string", seed = 42, shuffle=False,
                                                class_mode="categorical", target_size=(self.img_size,self.img_size), 
                                                batch_size=self.batch_size, subset = "training")

        
    def datagen_init_train(self, df, data_path): 
                                                                       
        abc = df.loc[df['feature_string'].isin(['Covid', 'Normal'])]
        abc = abc[0:7000]
        
        temp = dataGeneratorKerasV1(list(abc['Path']), list(abc['feature_string']))                                                             
        self.train_generator = temp
                                                                                                                             
    
    def datagen_init_valid(self, df, data_path):
                                                                       
        abc = df.loc[df['feature_string'].isin(['Covid', 'Normal'])]
        
        temp = dataGeneratorKerasV1(list(abc['Path']), list(abc['feature_string']),
                                   bs=32, shuffle=False, isRandRot=False, maxAngle=10.0, imgSz=224)                                                             
        self.validation_generator = temp                                                               
                  
import numpy as np
import pandas as pd
import os, ast
#import csv_parse
import classifier
from collections import Counter
from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "0,1"


def main():
    full_train_df = pd.read_pickle("../notebooks/fused_class_covid_chexpert_train_df.pkl")#[0:45000]    
    full_val_df = pd.read_pickle("../notebooks/fused_class_covid_chexpert_val_df.pkl")#[0:234]
    
    print(len(full_train_df))
    
    # Initialize classifier : <image_size> <num_gpus> <num_classes> <batch_size> <experiment_group> <experiment_title>

    classifier_covid = classifier.Classifier(320, 2, 10, 48, "COVID_CHEXPERT", "BCE_try2_")
    classifier_covid.datagen_init_train_old(full_train_df, "../data/")
    classifier_covid.datagen_init_valid_old(full_val_df, "../data/")
    
    classifier_covid.build_model(load_previous_weights=False, freeze_cnn=False)
    
    #print(classifier_covid.model.summary())

    history = classifier_covid.train_model(epochs=200)
    history.history

if __name__ == '__main__':
    main()

# 1. faster reduction
# 2. image range between 0 and 1
# 3. 
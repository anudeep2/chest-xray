import tensorflow as tf
from keras import backend as K
from keras.models import load_model
from tensorflow.python.compiler.tensorrt import trt_convert as trt
import os, sys
sys.path.insert(1, '../src/')

import classifier

def doit():
    classifier_8 = classifier.Classifier(224, 1, 9)
    classifier_8.build_model(load_previous_weights=False, freeze_cnn=False)
    model = classifier_8.model
    model.load_weights('../model_weights/Chex+Cov/9cls_BCE_sigmoid_32.hdf5')
    print([out.op.name for out in model.outputs])
    
    with tf.Session() as sess:
        # First deserialize your frozen graph:
        with tf.gfile.GFile('./model_frozen.pb', 'rb') as f:
            frozen_graph = tf.GraphDef()
            frozen_graph.ParseFromString(f.read())
        print("\n\nstep3\n\n")
        # Now you can create a TensorRT inference graph from your
        # frozen graph:
        converter = trt.TrtGraphConverter(
            input_graph_def=frozen_graph,
            nodes_blacklist=[out.op.name for out in model.outputs],
            precision_mode='FP32')  # output nodes
        print("\n\nstep4\n\n")
        trt_graph = converter.convert()
        print("\n\nstep5\n\n")
    tf.train.write_graph(trt_graph, './', 'trt.pb', as_text=False)
    print("\n\nstep6\n\n")
    
doit()
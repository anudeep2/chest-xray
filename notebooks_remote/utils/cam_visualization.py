import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import datetime
from pathlib import Path
import classifier

from sklearn.metrics import roc_auc_score
from sklearn.preprocessing import MultiLabelBinarizer

from sklearn.metrics import f1_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import roc_curve
from sklearn.metrics import auc
import sklearn

from skimage import filters
from skimage.measure import label, find_contours
import tensorflow as tf
import cv2

def _plot_cams(_desired_image_indices, _index, classifier_8, color_labels, label_map=None):
    # color labels: 0 -> green, 1 -> red, 2 -> blue, shape: num_samples * 3
    
    # create a 2x3 subplot
    num_plots = len(_desired_image_indices)
    num_rows = num_plots // 3
    if num_rows == 0 or num_rows == 1:
        num_rows = 2
    if num_plots > num_rows*3:
        num_rows += 1
    
    f, ax = plt.subplots(num_rows, 3, figsize=(30, num_rows*8))
    batch_size = classifier_8.batch_size

    for ctr, index in enumerate(_desired_image_indices):
        x = classifier_8.validation_generator[index//batch_size][0][index%batch_size]
        
        if label_map == None:
            CAT_CLASS_INDEX = _index
        else:
            CAT_CLASS_INDEX = label_map[_index]

        with tf.GradientTape() as tape:
            temp_list = classifier_8.model(np.expand_dims(x, axis=0))
            predictions = temp_list[0]
            conv_outputs = temp_list[-1]
            loss = predictions[:, CAT_CLASS_INDEX]

        output = conv_outputs[0]
        grads = tape.gradient(loss, conv_outputs)[0]


        gate_f = tf.cast(output > 0, 'float32')
        gate_r = tf.cast(grads > 0, 'float32')
        guided_grads = tf.cast(output > 0, 'float32') * tf.cast(grads > 0, 'float32') * grads

        weights = tf.reduce_mean(guided_grads, axis=(0, 1))

        cam = np.zeros(output.shape[0: 2], dtype = np.float32)

        for i, w in enumerate(weights):
            cam += w * output[:, :, i]    

        cam = cv2.resize(np.array(cam), (x.shape[0], x.shape[0]))
        cam = np.maximum(cam, 0)
        heatmap = (cam - cam.min()) / (cam.max() - cam.min())
        
        cam_raw = np.copy(cam)
        cam = cv2.applyColorMap(np.uint8(255*heatmap), cv2.COLORMAP_JET)
        
        preds_correct = "\n".join(color_labels[0][ctr])
        preds_incorrect = "\n".join(color_labels[1][ctr])
        not_preds = "\n".join(color_labels[2][ctr])
        
        ax[ctr//3][ctr%3].text(20, 0, preds_correct, ha="center", va="bottom", size="large",color="green")
        ax[ctr//3][ctr%3].text(90, 0, preds_incorrect, ha="center", va="bottom", size="large", color="red")
        ax[ctr//3][ctr%3].text(170, 0, not_preds, ha="center", va="bottom", size="large",color="blue")
        
        #ax[ctr//3][ctr%3].title.set_text(0.5, 1.2, a[counter],color=c[counter],fontsize=12,ha='left')
        ax[ctr//3][ctr%3].imshow((x - np.amin(x)) / (np.amax(x) - np.amin(x)))
        ax[ctr//3][ctr%3].imshow(cam, cmap="gray", alpha=0.2)
    
    plt.show()
    
    return 0
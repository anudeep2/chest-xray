import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import datetime
from pathlib import Path
import classifier

from sklearn.metrics import roc_auc_score
from sklearn.preprocessing import MultiLabelBinarizer

from sklearn.metrics import f1_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import roc_curve
from sklearn.metrics import auc
import sklearn
import random

def to_labels(pos_probs, threshold, if_multi_class=False):
    return (pos_probs >= threshold).astype('int')

def get_thresholds(y_labels, y_pred_keras, thresholds=None, tprs=None, 
                   mode='desired_recall', desired_recall=0.85):
    # <mode> : ['max_fscore', 'desired_recall']
    
    thresh = []
    if mode == 'max_fscore':
        for k in range(y_labels.shape[-1]):
            thresholds_n = np.arange(0, 1, 0.001)
            # evaluate each threshold
            scores = [f1_score(y_labels[:,k], to_labels(y_pred_keras[:,k], t)) for t in thresholds_n]
            ix = np.argmax(scores)
            thresh.append(float("%.2f"%thresholds_n[ix]))
            
        return thresh
        
    elif mode == 'desired_recall':        
        for k in range(y_labels.shape[-1]):
            curr_tpr = tprs[k]
            curr_thresh = thresholds[k]
            for ctr, i in enumerate(curr_tpr):
                if i > desired_recall[k]:
                    thresh.append(float("%.2f"%curr_thresh[ctr]))
                    break
        
        return thresh
        
    else:
        print("Error: Please choose the correct model")
        return -1
    
    
def _classification_report(y_labels, y_pred_keras, thresholds):
    thresh = thresholds
    sen = []
    spec = []
    pos = []
    neg = []
    fns = []
    fps = []
    confs = []

    for k in range(len(thresh)):
        y_pred = np.copy(y_pred_keras[:, k])
        y_pred[y_pred > thresh[k]] = 1
        y_pred[y_pred != 1] = 0
        conf = confusion_matrix(y_labels[:, k], y_pred)
        tn, fp, fn, tp = conf.ravel()

        confs.append(conf)
        pos.append(float("%.0f"%(tp+fn)))
        neg.append(float("%.0f"%(fp+tn)))
        fns.append(float("%.0f"%fn))
        fps.append(float("%.0f"%fp))
        sen.append(float("%.2f"%(tp/(tp+fn))))
        spec.append(float("%.2f"%(tn/(tn+fp))))
        
    return sen, spec, pos, neg, fns, fps, confs

def binarize_labels(classifier):
    test = pd.Series(classifier.validation_generator.labels)
    mlb = MultiLabelBinarizer()
    y_labels = mlb.fit_transform(test)
    
    return y_labels

def get_classification_metrics(y_labels, y_pred_keras):
    ROC_AUCs = []
    PR_AUCs = []
    thresholds = []
    fprs = []
    tprs = []

    for ii in range(y_pred_keras.shape[1]):
        fpr_keras, tpr_keras, thresholds_keras = roc_curve(y_labels[:,ii], y_pred_keras[:,ii])
        auc_keras = auc(fpr_keras, tpr_keras)
        ROC_AUCs.append(auc_keras)
        thresholds.append(thresholds_keras)
        fprs.append(fpr_keras)
        tprs.append(tpr_keras)
        precision, recall, _ = sklearn.metrics.precision_recall_curve(y_labels[:, ii], y_pred_keras[:, ii])
        auc_keras = auc(recall, precision)
        PR_AUCs.append(auc_keras)
        
    return ROC_AUCs, PR_AUCs, thresholds, fprs, tprs


def get_indices(_index, _error_type, y_labels, y_pred_keras, thresholds=None, if_all=False, ret_num=6):
    if thresholds == None:
        thresholds = get_thresholds(y_labels, y_pred_keras, mode='max_fscore')
    else:
        thresholds = thresholds
    thresh_class = thresholds[_index]
    
    y_pred = np.copy(y_pred_keras[:, _index])
    y_pred[y_pred > thresh_class] = 1
    y_pred[y_pred != 1] = 0
    conf = confusion_matrix(y_labels[:, _index], y_pred)
    tn, fp, fn, tp = conf.ravel()
    
    if _error_type == 'FN':
        # y_label = 1, y_pred = 0
        num_samples = fn
        
        if num_samples > ret_num and not if_all:
            _error_select_indices = random.sample(range(num_samples), ret_num)
            msg = "%d of %d false negatives will be shown"%(ret_num,num_samples)
        else:
            _error_select_indices = random.sample(range(num_samples), num_samples)
            msg = "all false negatives (%d) will be shown"%num_samples
       
        _error_indices = []
        for k in range(y_labels.shape[0]):
            if y_labels[k, _index] == 1 and y_pred[k] == 0:
                _error_indices.append(k)
        
        return_list = []
        for k in _error_select_indices:
            return_list.append(_error_indices[k])
                 
    
    elif _error_type == 'FP':
        # y_label = 0, y_pred = 1
        num_samples = fp
        
        if num_samples > ret_num and not if_all:
            _error_select_indices = random.sample(range(num_samples), ret_num)
            msg = "%d of %d false positives will be shown"%(ret_num, num_samples)
        else:
            _error_select_indices = random.sample(range(num_samples), num_samples)
            msg = "all false positives (%d) will be shown"%num_samples
       
        _error_indices = []
        for k in range(y_labels.shape[0]):
            if y_labels[k, _index] == 0 and y_pred[k] == 1:
                _error_indices.append(k)
        
        return_list = []
        for k in _error_select_indices:
            return_list.append(_error_indices[k])
            
    return return_list, thresholds, msg


def to_colorful_label_predictions(desired_image_indices, y_labels, y_pred_keras, thresholds, class_names):
    # returns [predictions correct], [predictions incorrect], [not predicted]
    
    predictions_correct = []
    predictions_incorrect = []
    not_predicted = []
    
    for k in desired_image_indices:
        preds_correct = []
        preds_incorrect = []
        not_preds = []
        
        y_label = y_labels[k]
        y_pred = [0]*y_labels.shape[-1]
        for ctr, l in enumerate(thresholds):
            if y_pred_keras[k][ctr] > l:
                y_pred[ctr] = 1
        
        for ctr in range(y_labels.shape[-1]):
            if y_label[ctr] == 1 and y_pred[ctr] == 1:
                preds_correct.append(class_names[ctr]+':%.2f'%y_pred_keras[k][ctr])
            
            elif y_label[ctr] == 0 and y_pred[ctr] == 1:
                preds_incorrect.append(class_names[ctr]+':%.2f'%y_pred_keras[k][ctr])
                
            elif y_label[ctr] == 1 and y_pred[ctr] == 0:
                not_preds.append(class_names[ctr]+':%.2f'%y_pred_keras[k][ctr])
                
        predictions_correct.append(preds_correct)
        predictions_incorrect.append(preds_incorrect)
        not_predicted.append(not_preds)
        
    return predictions_correct, predictions_incorrect, not_predicted
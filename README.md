# Chest-Xray

This script uses densenet 121 to perform multi class respiratory disease classification from xray images. The prerequsites are as follow-
```
keras 2.x
numpy 15.x+
pandas
sci-kit learn
opencv-python
```
## Run a training session

Trigger a training sesssion by running 
`python src/train.py`

The required data references are hard-coded in the `train.py` file. They can be store in CSVs or pandas dataframes with the following structure

```
Path, feature_string
00001.png, COVID-19
00002.png, Pneumonia
...
...
...
```

The validation scripts can understand the model performance and visualize results. They are self-explanatory and can be found in the notebooks_remote directory.

## Dataset and Current training runs

We've used this setup to train model for two different problems
- Chexpert 
    - This dataset is on the Warehouse machine in `/home/ubuntu/anudeep/data`. In case that it was deleted, it can be readily found online at https://stanfordmlgroup.github.io/competitions/chexpert/
    - It covers 14 diagnosis, but we collapsed these to 10 needed. They are as follows
        - ```'Atelectasis_Pneumothorax', 'COVID-19', 'Cardiac Abnormality', 'Consolidation', 'Edema', 'Lung Opacity', 'Healthy', 'Pleural Abnormality', 'Support Devices', 'pneumonia'```
    - The weights for a densenet121 trained can be found on the depot machine at 
        - `/home/ubuntu/anudeep/covid19/model_weights/COVID_CHEXPERT`

- Covid19 data from various sources
    - This dataset was used to train a multi-class setting for three labels
        - `COVID-19, Pneumonia, Normal`  
    - The weights for a trained model can be found at (on depot machine)
        - `/home/ubuntu/anudeep/covid19/model_weights/multi_class`

